.. GolemOS documentation master file, created by
   sphinx-quickstart on Thu Mar 18 16:44:18 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GolemOS documentation!
===================================


The object of the project is Golem - a humanoid robot equipped with numerous modules combining mechanics, electronics and cybernetics. Golem's originators are a part of Smart Control Labs (SCLabs). Currently, the robot is in the form of a torso with an attached arm, whose movement is provided by a custom system of hydraulic muscles.

The task of the project team is to design and build the foundation of the visual perception module. With the help of a depth camera, the robot will recognize objects and react accordingly to changes in the environment. Operating system image with installed necessary drivers and programs will be created with the help of the Yocto project, then tested on a virtual machine (QEMU), Raspberry Pi platform and finally Jetson Nano. 

The expected result is a working, personalized operating system on the NVIDIA's Jetson Nano platform. Moreover, the synchronization of later implemented robot's tasks will be based on the ROS platform, therefore the group's task is also to make a choice between ROS1 and ROS2 (and justify it). 
ROS indeed does support QEMU and RPi4, but (officially) does not support Jetson Nano, so we hope to make a contribution to this open source tool and expand the list of supported platforms.

.. toctree::
   :maxdepth: 2
   :caption: Project phases:
   
   qemu
   rpi
   jetson
