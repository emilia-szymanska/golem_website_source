Poky on RPi
************

.. toctree::
   :maxdepth: 2
   :caption: Project phases:
   

.. note:: We have experimented with **Raspberry Pi 3** and **Raspberry Pi 4**. The presented steps consider RPi4B. It is also assumed that you are familiar with the previous QEMU section. 

===================
RPi basic OS
===================


Steps to build Poky 
-------------------


1. Make sure that your computer is able to run the bitbake building. That is why the following tools must be downloaded:
        
::

        $ sudo apt-get install gawk wget git-core diffstat unzip texinfo \
            gcc-multilib build-essential chrpath socat libsdl1.2-dev xterm bmap-tools \
            make xsltproc docbook-utils fop dblatex xmlto cpio python python3 \
            python3-pip python3- pexpect xz-utils debianutils iputils-ping python-git \
            bmap-tools python3-git curl parted dosfstools mtools gnupg autoconf \
            automake libtool libglib2.0- dev python-gtk2 bsdmainutils screen \
            libstdc++-5-dev libx11-dev
        
        
            
2. Clone Poky distribution and relevant meta-layers (structures that contain classes, recipes and conf files):
::      

        $ git clone --single-branch -b zeus git://git.yoctoproject.org/poky.git
        $ cd poky
        $ git clone --single-branch -b zeus git://git.yoctoproject.org/meta-raspberrypi
        $ git clone --single-branch -b zeus git://git.openembedded.org/meta-openembedded
        
.. tip:: After *-b* flag you are supposed to put the name of the Poky release you want to use. We used Zeus release here, Gatesgarth in QEMU steps.

3. Run the oe-init-build-env script, that defines OpenEmbedded build environment settings:
::
        
        $ source oe-init-build-env

        
4. Next step is different from qemu configuration. Modify file:`local.conf` as below:
   
::

        # Machine Selection
        MACHINE ?= "raspberrypi4-64"

        # The distribution setting controls which policy settings are used as defaults.
        DISTRO ?= "poky"

        # Package Management configuration
        PACKAGE_CLASSES ?= "package_rpm"

        # The EXTRA_IMAGE_FEATURES variable allows extra packages to be added to the generated images.
        EXTRA_IMAGE_FEATURES ?= "debug-tweaks"

        # Additional image features
        USER_CLASSES ?= "buildstats image-mklibs image-prelink"

        # Interactive shell configuration
        PATCHRESOLVE = "noop"

        # Disk Space Monitoring during the build
        BB_DISKMON_DIRS ??= "\
                STOPTASKS,${TMPDIR},1G,100K \
                STOPTASKS,${DL_DIR},1G,100K \
                STOPTASKS,${SSTATE_DIR},1G,100K \
                STOPTASKS,/tmp,100M,100K \
                ABORT,${TMPDIR},100M,1K \
                ABORT,${DL_DIR},100M,1K \
                ABORT,${SSTATE_DIR},100M,1K \
                ABORT,/tmp,10M,1K"

        # configuration version
        CONF_VERSION = "1"
   
.. tip:: Depending on what device we have, option „MACHINE” ought to be related to it. In the directory *./poky/meta-raspberrypi/conf/machine* we can see all of the possible machines. We tested this part on RPi4, so we use raspberrypi4-64 metalayer.
        
5. Modify :file:`conf/bblayes.conf` by running the following commands:
::

        $ bitbake-layers add-layer ../meta-raspberrypi
        $ bitbake-layers add-layer ../meta-openembedded/meta-oe
        
6. Build an OS image through bitbake command:
::

        $ bitbake core-image-minimal
        
Unfortunatelly it may take a long time. After successful building the image, it ought to appear at *./tmp/deploy/images/raspberry3-64 directory*:  file:`core-image-minimal-raspberrypi4-64-*.rootfs.wic.bz2`.
        
7. Save created image on SD card (be cautious which partition is associated with the card):
::

        $ cd ./tmp/deploy/images/raspberrypi4-64
        $ sudo bmaptool core-image-minimal-raspberrypi4-64-*.rootfs.wic.bz2 /dev/${your_device_name}





Results 
-------

We booted the system image on RPi3B and this process is presented below.

.. image:: img/rpi_general.jpg
  :align: center
  :height: 600

We can now execute basic bash commands in the shell!

.. image:: img/rpi_boot.jpg
  :width: 800


===========================================================
RPi OS with ROS and camera drivers and visualization script
===========================================================


Brief comparison between ROS and ROS2
---------------------------------------

We needed to decide whether to use ROS or ROS2 in our future vision system. We have gathered information on the differences between them and put them in the table below. 



.. list-table:: 
   :widths: 50 50
   :header-rows: 1
   :align: center

   * - Robot Operating System 
     - Robot Operating System 2
   * - widely known for a long time
     - relatively new
   * - a lot of tools adjusted for ROS
     - tools still being transferred from ROS to ROS2
   * - API not necessarily the same between roscpp and rospy 
     - one base library (rcl) with similar API between rclcpp and rclpy (easy to develop rcljava, rclnodejs etc.)
   * - targeting C++98 
     - targeting C++11 and C++14
   * - freedom in node implementation 
     - modular structure for writing a node
   * - launch files in XML 
     - launch files in Python
   * - need to run the ROS master 
     - nodes are independent and not tight to a global master
   * - parameters handled by the parameter server
     - node declares and manages its own parameters 
   * - synchronous services 
     - asynchronous services 
   * - catkin build system 
     - ament build system with colcon tool 
   * - Ubuntu as a main OS target
     - Ubuntu, MacOS and Windows 10 as OS targets 




Steps to build Poky 
-------------------


1. Make sure that your computer is able to run the bitbake building. That is why the following tools must be downloaded:
        
::

        $ sudo apt-get install gawk wget git-core diffstat unzip texinfo \
            gcc-multilib build-essential chrpath socat libsdl1.2-dev xterm bmap-tools \
            make xsltproc docbook-utils fop dblatex xmlto cpio python python3 \
            python3-pip python3- pexpect xz-utils debianutils iputils-ping python-git \
            bmap-tools python3-git curl parted dosfstools mtools gnupg autoconf \
            automake libtool libglib2.0- dev python-gtk2 bsdmainutils screen \
            libstdc++-5-dev libx11-dev
        
        
            
2. Clone Poky distribution and relevant meta-layers (structures that contain classes, recipes and conf files), here for Hardknott distro:
::      

		$ git clone --single-branch -b hardknott https://git.yoctoproject.org/git/poky
		$ cd poky
		$ git clone --single-branch -b hardknott https://github.com/IntelRealSense/meta-intel-realsense.git
		$ git clone --single-branch -b hardknott https://git.yoctoproject.org/git/meta-raspberrypi
		$ git clone --single-branch -b hardknott https://github.com/openembedded/meta-openembedded.git
		$ git clone --single-branch -b hardknott https://github.com/ros/meta-ros.git

        
.. tip:: After *-b* flag you are supposed to put the name of the Poky release you want to use. We used Hardknott release here, Gatesgarth in QEMU steps.

3. Put our custom `meta-layer <https://gitlab.com/emilia-szymanska/golem_website_source/-/tree/master/meta-depthcamera-setup>`_ inside the poky directory.

4. Run the oe-init-build-env script, that defines OpenEmbedded build environment settings:
::
        
        $ source oe-init-build-env

.. warning:: The command above will change your location to the build folder

5. Modify :file:`conf/bblayes.conf` by executing the following commands:
::

		$ bitbake-layers add-layer ../meta-raspberrypi
		$ bitbake-layers add-layer ../meta-openembedded/meta-oe
		$ bitbake-layers add-layer ../meta-openembedded/meta-python 
		$ bitbake-layers add-layer ../meta-openembedded/meta-networking 
		$ bitbake-layers add-layer ../meta-openembedded/meta-gnome
		$ bitbake-layers add-layer ../meta-intel-realsense 
		$ bitbake-layers add-layer ../meta-ros/meta-ros-common 
		$ bitbake-layers add-layer ../meta-ros/meta-ros2 
		$ bitbake-layers add-layer ../meta-ros/meta-ros2-foxy
		$ bitbake-layers add-layer ../meta-depthcamera-setup 

6. Remove librealsense2 recipe from meta-ros layer:
::

		$ rm -rf ../meta-ros/meta-ros2-foxy/generated-recipes/librealsense2
		$ rm -rf ../meta-ros/meta-ros2-foxy/recipes-bbappends/librealsense

.. warning::  The step above is crucial as the meta-ros librealsense2 recipe is in direct conflict with the librealsense2 recipe from meta-intel-realsense layer which is the one to be installled on final image.

7. Modify file:`conf/local.conf` to be the same as shown below:
::

		# Machine Selection
		MACHINE ?= "raspberrypi4-64"

		IMAGE_FSTYPES = "tar.xz ext3 rpi-sdimg"

		IMAGE_INSTALL_append = " python3 python3-pip python3-opencv zsh curl gnome-common librealsense2 python3-pyrealsense2 ros-core camera-viewer"

		DISTRO_FEATURES_append = " systemd"

		VIRTUAL-RUNTIME_init_manager = "systemd"
		VIRTUAL-RUNTIME_initscript = "systemd-compat-units"

		MACHINE_EXTRA_RDEPENDS_append = " kernel-module-uvcvideo"

		PACKAGECONFIG_pn-librealsense2 = "rsusb"

		# The distribution setting controls which policy settings are used as defaults.
		DISTRO ?= "poky"

		# Package Management configuration
		PACKAGE_CLASSES ?= "package_rpm"

		# The EXTRA_IMAGE_FEATURES variable allows extra packages to be added to the generated images.
		EXTRA_IMAGE_FEATURES ?= "debug-tweaks"

		# Additional image features
		USER_CLASSES ?= "buildstats image-mklibs image-prelink"

		# Interactive shell configuration
		PATCHRESOLVE = "noop"

		# Disk Space Monitoring during the build
		BB_DISKMON_DIRS ??= "\
				STOPTASKS,${TMPDIR},1G,100K \
				STOPTASKS,${DL_DIR},1G,100K \
				STOPTASKS,${SSTATE_DIR},1G,100K \
				STOPTASKS,/tmp,100M,100K \
				ABORT,${TMPDIR},100M,1K \
				ABORT,${DL_DIR},100M,1K \
				ABORT,${SSTATE_DIR},100M,1K \
				ABORT,/tmp,10M,1K"

		# configuration version
		CONF_VERSION = "1"
   
.. tip:: Depending on what device we have, option „MACHINE” ought to be related to it. In the directory *./poky/meta-raspberrypi/conf/machine* we can see all of the possible machines. We tested this part on RPi3, so we use raspberrypi3-64 metalayer.
         
8. Build an OS image through bitbake command:
::

		$ bitbake core-image-sato
        
Unfortunatelly it may take a long time. After successful building the image, it ought to appear at *./tmp/deploy/images/raspberry3-64 directory*:  file:`core-image-sato-raspberrypi4-64-*.rootfs.rpi-sdimg`.
        
9. Save created image on SD card (be cautious which partition is associated with the card):
::

        $ cd ./tmp/deploy/images/raspberrypi4-64
        $ sudo dd if=core-image-sato-raspberrypi4-64-*.rootfs.rpi-sdimg of=/dev/${your_device_name} bs=10M status=progress conv=sync

Running the visualization script
--------------------------------

In order to run the script displaying camera output with face detection simply run the following command:
::

		$ camera-viewer.py

To automatically run the script after the system is up it is required to create and enable systemd script. How it can be done is described by the following steps:

1. Create file named <name of your service>.service in /etc/systemd/system which contains:
::

		[Unit]
		Description="Service starting the depth camera vizualization script"
		PartOf=graphical-session.target
		Requires=graphical.target
		StartLimitBurst = 10
		StartLimitIntervalSec = 5

		[Service]
		Environment="DISPLAY=:0"
		ExecStart=/usr/bin/camera-viewer.py
		Restart=always
		RestartSec = 1
		
		[Install]
		WantedBy=xsession.target 

2. Execute the following commands:
::

		$ sudo systemctl daemon-reload
		$ sudo systemctl enable <name of your service>.service
		$ sudo systemctl start <name of your service>.service
