Poky on QEMU
************

.. toctree::
   :maxdepth: 2
   :caption: Project phases:
   

==========
Used tools 
==========

To configure and build operating system for virtual machine qemu we have used the following tools and documents:

1. Yocto Project - `Poky repository`_ - open source full-platform build tool based on Linux; 
2. Yocto Project - `Mega-Manual`_ - step by step instructions.

The Poky repository itself uses the following open source tools:

1. `OpenEmbedded`_ - environment for automating Linux compilation on embedded systems; 
2. `BitBake`_ - tool for building and distributing Linux packets;
3. `QEMU`_ - virtual machine(Quick EMUlator).


.. _Poky repository: https://git.yoctoproject.org/cgit/cgit.cgi/poky/
.. _Mega-Manual: https://www.yoctoproject.org/docs/current/mega-manual/mega-manual.html

.. _OpenEmbedded: https://www.openembedded.org/wiki/Main_Page
.. _BitBake: https://www.yoctoproject.org/docs/1.6/bitbake-user-manual/bitbake-user-manual.html
.. _QEMU: https://www.qemu.org/



===================
Steps to build Poky 
===================

Make sure your computer is able to run the bitbake building - we used a server with 24 cores because on our laptops the build would take even 7 hours. 

1. First of all, download obligatory tools like Python3 (3.5.0 or greater), tar (1.28 or greater), gcc (5.0 or greater) and Git (1.8.3.1 or greater).

2. Clone repository from git:
::

        $ git clone git://git.yoctoproject.org/poky

3. Take a look at the tags:
::

        $ git tag
        1.1_M1.final
        1.1_M1.rc1
        .
        .
        .

The tag that interests us is the newest gatesgarth. Do the git checkout to this branch.
::
        
        $ git tag | grep 24
        $ git checkout gatesgarth-24.0.2

4. In the poky directory run the oe-init-build-env script, that defines OpenEmbedded build environment settings and adds the build directory:
::

        $ cd ~/poky
        $ source oe-init-build-env


5. Apply changes to the :file:`local.conf` file to specify your desired features. This is how our configuration file looked like:
::

        # Machine Selection
        MACHINE ??= "qemux86-64"

        # The distribution setting controls which policy settings are used as defaults.
        DISTRO ?= "poky"

        # Package Management configuration
        PACKAGE_CLASSES ?= "package_rpm"

        # The EXTRA_IMAGE_FEATURES variable allows extra packages to be added to the generated images.
        EXTRA_IMAGE_FEATURES ?= "debug-tweaks"

        # Additional image features
        USER_CLASSES ?= "buildstats image-mklibs image-prelink"

        # Interactive shell configuration
        PATCHRESOLVE = "noop"

        # Disk Space Monitoring during the build
        BB_DISKMON_DIRS ??= "\
                STOPTASKS,${TMPDIR},1G,100K \
                STOPTASKS,${DL_DIR},1G,100K \
                STOPTASKS,${SSTATE_DIR},1G,100K \
                STOPTASKS,/tmp,100M,100K \
                ABORT,${TMPDIR},100M,1K \
                ABORT,${DL_DIR},100M,1K \
                ABORT,${SSTATE_DIR},100M,1K \
                ABORT,/tmp,10M,1K"

        # Qemu configuration
        PACKAGECONFIG_append_pn-qemu-system-native = " sdl"

        # configuration version
        CONF_VERSION = "1"



6. Build an OS image through bitbake command:
::

        $ bitbake core-image-minimal

The execution of this command takes quite a lot of time. Fortunately, the SCLabs' server could do it much faster using more cores and RAM.

7. If you had no warnings/errors, you can notice a directory that contains our OS image in */poky/build/tmp/deploys/images*. It should be :file:`qemux86-64`. To run this system type:
::

        $ runqemu nographic qemux86-64

Nographic flag is needed due to the form of this system (it has no GUI).
According to default setting login ought to be "root".


=======
Results 
=======

The result of the first phase is a working system on x86-64 architecture. We booted the system image with QEMU and this process (alongside with an example of a failed logging) is presented below.

.. image:: img/qemu_boot.png
  :width: 800

We can now execute basic bash commands in the shell!

.. image:: img/qemu_msg.png
  :width: 800
