DESCRIPTION =  "Python script for displayinG camera depth and color output with basic face detection."

SECTION = "scripts"
RDEPENDS_${PN} += " python3-numpy python3-opencv python3-pyrealsense2"
DEPENDS = ""
LICENSE = "CLOSED"


SRC_URI = "file://camera-viewer.py \
	   file://haarcascade_frontalface_default.xml \
          "
FILES_${PN} += "/opencv-classifiers/*" 

do_install() {
install -d ${D}${bindir}
install ${WORKDIR}/camera-viewer.py ${D}${bindir}

install -d ${D}/opencv-classifiers
install ${WORKDIR}/haarcascade_frontalface_default.xml ${D}/opencv-classifiers
}
