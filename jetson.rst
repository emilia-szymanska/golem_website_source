Poky on Jetson Nano
*******************

.. toctree::
   :maxdepth: 2
   :caption: Project phases:
   

.. note:: We have experimented with **Jetson Nano Developer Kit**. It is also assumed that you are familiar with the previous QEMU and RPi section. 

.. note:: Before attampting to bitbake jetson image it is required to download and configure SDK for Jetson Nano. Keep in mind that SDK needs GUI to run.

===================
Jetson basic OS
===================


Steps to build Poky 
-------------------


1. Make sure that your computer is able to run the bitbake building. That is why the following tools must be downloaded:
        
::

        $ sudo apt-get install gawk wget git-core diffstat unzip texinfo \
               gcc-multilib build-essential chrpath socat libsdl1.2-dev xterm bmap-tools
        

2. Create directory for your project (and temporary directories as *yocto_tmp*) and clone Poky distribution:
::

        $ mkdir yocto-jetson
        $ cd yocto-jetson
        $ mkdir -p yocto_tmp/sstate_dir yocto_tmp/downloads
        $ git clone --single-branch -b dunfell git://git.yoctoproject.org/poky.git
        $ cd poky
        
.. tip:: After *-b* flag you are supposed to put the name of the Poky release you want to use. We used Dunfell release here, Zeus in RPi, Gatesgarth in QEMU steps.
               
3. Clone relevant meta-layers (structures that contain classes, recipes and conf files):
::

        $ git clone --single-branch -b dunfell-l4t-r32.4.3 https://github.com/madisongh/meta-tegra.git
        

4. Run the oe-init-build-env script, that defines OpenEmbedded build environment settings:
::
        
        $ source oe-init-build-env

        
5. Modify file:`local.conf` as below:
   
::

        # Machine Selection
        MACHINE ?= "jetson-nano-qspi-sd"

        # The distribution setting controls which policy settings are used as defaults.
        DISTRO ?= "poky"

        IMAGE_CLASSES += "image_types_tegra"
        IMAGE_FSTYPES = "tegraflash"

        SSTATE_DIR ?= "/home/${USER}/yocto-jetson/yocto_tmp/sstate_dir"
        DL_DIR ?= "/home/${USER}/yocto-jetson/yocto_tmp/downloads"

        
        # Package Management configuration
        PACKAGE_CLASSES ?= "package_rpm"

        # The EXTRA_IMAGE_FEATURES variable allows extra packages to be added to the generated images.
        EXTRA_IMAGE_FEATURES ?= "debug-tweaks"

        # Additional image features
        USER_CLASSES ?= "buildstats image-mklibs image-prelink"

        # Interactive shell configuration
        PATCHRESOLVE = "noop"

        # Disk Space Monitoring during the build
        BB_DISKMON_DIRS ??= "\
                STOPTASKS,${TMPDIR},1G,100K \
                STOPTASKS,${DL_DIR},1G,100K \
                STOPTASKS,${SSTATE_DIR},1G,100K \
                STOPTASKS,/tmp,100M,100K \
                ABORT,${TMPDIR},100M,1K \
                ABORT,${DL_DIR},100M,1K \
                ABORT,${SSTATE_DIR},100M,1K \
                ABORT,/tmp,10M,1K"

        # configuration version
        CONF_VERSION = "1"
   
        
6. Modify :file:`conf/bblayes.conf`:
::

        BBLAYERS ?= " \         
                /home/${USER}/yocto-tegra/meta-tegra \
                /home/${USER}/yocto-tegra/poky-dunfell/meta \
                /home/${USER}/yocto-tegra/poky-dunfell/meta-poky \
                /home/${USER}/yocto-tegra/poky-dunfell/meta-yocto-bsp \
                "
        
7. Build an OS image through bitbake command:
::

        $ bitbake core-image-base
        
8. After successful image building, in build directory:
::

        $ cd tmp/work/jetson_nano_gspi_sd-poky-linux/core-image-base/1.0-r0/tegraflash
        $ ./dosdcard.sh
        
9. Save created image on SD card (be cautious which partition is associated with the card):
::

        $ sudo dd if=core-image-base.sdcard of=/dev/sdd bs=10M status=progress

